#$cfg = gc .\objects\bis_windows_servers.cfg

$arguments = @("-l",
				"nagremote",
				"-i",
				".\remote.ppk",
				"bis-nagios.bussops.ubc.ca",
				"-batch",
				"cat /usr/local/nagios/etc/objects/bis_windows_servers.cfg")

start-process -filepath ".\plink.exe" -argumentlist $arguments -wait -nonewwindow -RedirectStandardOutput ".\temp_cfg.txt"

$cfg = gc ".\temp_cfg.txt"
rm ".\temp_cfg.txt"
				
$host_table = @{}

for ($i=0; $i -lt $cfg.count; $i++) {
	if ($cfg[$i] -match "define host{"){
		$temp_index = 0
		$object_host = new-object system.object
		$temp_hostname = $null
		for ($x=$i+1; $cfg[$x] -notmatch "}"; $x++) {
			$temp_string = $cfg[$x] -split '\s+',3
			$key = $temp_string[1].trim()
			if ($temp_string[2].indexof(";") -ne -1) {
				$value = ($temp_string[2].substring(0,$temp_string[2].indexof(";"))).trim()
			} else {
				$value = ($temp_string[2]).trim()
			}
			if ($key -match "host_name") {
				$temp_hostname = $value
			}
			#Write-Host "KEY: $key  VALUE: $value"
			if ($key -ne ";" ) {
				$object_host | Add-member -type NoteProperty -name $key -value $value
			}
			$temp_index = $x
		}
		$host_table.add($temp_hostname,$object_host)
		$i = $temp_index
	}
}
foreach ($thing in $host_table.getenumerator()) {($thing.value).host_name}