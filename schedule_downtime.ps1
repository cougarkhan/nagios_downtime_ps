param (
		[alias("h")]
		[string]$host_name,
		[alias("l")]
		[switch]$local_host,
		[alias("s")]
		[switch]$services,
		[alias("d")]
		[string]$duration="900", 
		[alias("c")]
		[string]$comment="UNDEFINED",
		[alias("i")]
		[string]$input_file="$scriptpath\hosts.txt")

$author = (gci env:USERNAME).value
$scriptpath = $MyInvocation.MyCommand.Path
$scriptpath = Split-Path $scriptpath

if ($services) {
	$type = 2
} else {
	$type = 1
}

if ($local_host -eq $true) {
	$machines = (gci env:COMPUTERNAME).value
} else {
	if (!$host_name) {
		if(!(test-path $input_file)) {
			Write-host "$([char]34)$input_file$([char]34) does not exist!"
			read-host "Press Enter to exit..."
			exit
		} else {
			$machines = gc $input_file
			if ($machines -eq $null) {
				Write-Host "No hosts specified at command line or within hosts.txt!"
				read-host "Press Enter to exit..."
				exit
			}
		}
	} else {
		$machines = $host_name
	}
}

$machines | %{ 
	$machine_name = $_.ToUpper()
	$arguments = @("-l",
					"nagremote",
					"-i",
					"$scriptpath\remote.ppk",
					"bis-nagios.bussops.ubc.ca",
					"-batch",
					"/home/nagremote/nagios-downtimes/host-downtime.sh",
					"$type",
					"$machine_name",
					"$duration",
					"$author",
					"$([char]92)$([char]34)$comment$([char]92)$([char]34)")
	start-process -filepath "$scriptpath\plink.exe" -argumentlist $arguments -nonewwindow -wait

	$c = 8
	do {
		write-host "Waiting for $c seconds...`r" -nonewline
		sleep -seconds 1
		$c--
	} while ($c -gt 0)
	
	# for ($c=5; $c -gt 0; $c--) {
		# write-host "Waiting for $c seconds...`r" -nonewline
		# sleep -seconds 1
	# }
	write-host "`n"

	$arguments_host = @("-l",
					"nagremote",
					"-i",
					"$scriptpath\remote.ppk",
					"bis-nagios.bussops.ubc.ca",
					"-batch",
					"grep -A 11 -i $([char]92)$([char]34)hostdowntime$([char]92)$([char]34) /usr/local/nagios/var/status.dat")
					
	start-process -filepath "$scriptpath\plink.exe" -argumentlist $arguments_host -wait -nonewwindow -RedirectStandardOutput "$scriptpath\temp_host.txt"
	$host_downtime_results = gc "$scriptpath\temp_host.txt"
	rm "$scriptpath\temp_host.txt"

	for($i=0; $i -lt $host_downtime_results.count; $i++) {
		if($host_downtime_results[$i].trim().startswith("hostdowntime") -AND $host_downtime_results[$i+1].trim().startswith("host_name") -AND $host_downtime_results[$i+1].contains($machine_name)) {
			
			$object_host = new-object system.object
			
			for($x=$i+1;($host_downtime_results[$x].contains("}")) -ne $true; $x++) {
				$values = $host_downtime_results[$x].trim().split("=")
				$object_host | Add-member -type NoteProperty -name $values[0] -value $values[1]
				$i=$x
			}
			$entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.entry_time))

			if (($(date) - $entry_time).totalseconds -lt 30) {
				write-host "Recent Host downtime entry Found" -foregroundcolor GREEN
				write-host "Host Downtime has been set for $machine_name" -foregroundcolor GREEN
				$object_host.entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.entry_time))
				$object_host.start_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.start_time))
				$object_host.end_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_host.end_time))
				$object_host
			} else {
				write-host "Found a previousley scheduled host downtime for $machine_name." -foregroundcolor RED
			}
			
			$object_host = $null
		}
	}

	if ($services) {
		$arguments_service = @("-l",
						"nagremote",
						"-i",
						"$scriptpath\remote.ppk",
						"bis-nagios.bussops.ubc.ca",
						"-batch",
						"grep -A 12 -i $([char]92)$([char]34)servicedowntime$([char]92)$([char]34) /usr/local/nagios/var/status.dat")

		start-process -filepath "$scriptpath\plink.exe" -argumentlist $arguments_service -wait -nonewwindow -RedirectStandardOutput "$scriptpath\temp_service.txt"
		$service_downtime_results = gc "$scriptpath\temp_service.txt"
		rm "$scriptpath\temp_service.txt"

		for($i=0; $i -lt $service_downtime_results.count; $i++) {
			if($service_downtime_results[$i].trim().startswith("servicedowntime") -AND $service_downtime_results[$i+1].trim().startswith("host_name") -AND $service_downtime_results[$i+1].contains($machine_name)) {
				
				$object_service = new-object system.object
				
				for($x=$i+1;($service_downtime_results[$x].contains("}")) -ne $true; $x++) {
					$values = $service_downtime_results[$x].trim().split("=")
					$object_service | Add-member -type NoteProperty -name $values[0] -value $values[1]
					$i=$x
				}
				$entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.entry_time))

				if (($(date) - $entry_time).totalseconds -lt 30) {
					write-host "Recent service downtime entry Found" -foregroundcolor GREEN
					write-host "Service Downtime has been set for $machine_name" -foregroundcolor GREEN
					$object_service.entry_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.entry_time))
					$object_service.start_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.start_time))
					$object_service.end_time = [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($object_service.end_time))
					$object_service
				} else {
					write-host "Found a previousley scheduled service downtime for $machine_name." -foregroundcolor RED
				}
				
				$object_service = $null
			}
		}
	}
}

read-host "Press Enter to exit..."